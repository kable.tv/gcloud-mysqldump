FROM alpine:3.4
RUN apk add --update mysql-client bash openssh-client && rm -rf /var/cache/apk/*
ADD https://storage.googleapis.com/pub/gsutil.tar.gz /tmp/gsutil.tar.gz
RUN tar xfz /tmp/gsutil.tar.gz -C /root && \
    rm -f  /tmp/gsutil.tar.gz
ENTRYPOINT ["/bin/sh", "-c"]
CMD ["bash"]
